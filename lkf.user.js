// ==UserScript==
// @name        lkf
// @namespace   riivo
// @include     https://www.lkf.ee/et/kahjukontroll*
// @version     3
// ==/UserScript==

let reg, vin
(vin = new URL(window.location).searchParams.get('vin')) &&
  (document.getElementById('edit-vehicle').value = vin) ||
  (reg = new URL(window.location).searchParams.get('reg')) &&
  (document.getElementById('edit-vehicle').value = reg)
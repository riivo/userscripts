// ==UserScript==
// @name        tty
// @namespace   riivo
// @include     https://ois2.ttu.ee/uusois/!uus_ois2.ois_public.show_win?_page=*&p_ryhm=*&p_liik=S&p_show=0&p_tyyp=3*
// @version     4
// ==/UserScript==

document.querySelectorAll('[id^=tunnPaev]').forEach(day => {
  const divDate = day.innerText.split(', ')[1].split('.')
  const divTime = Date.parse(`${divDate[1]}/${divDate[0]}/${divDate[2]}`) + 864e5
  day.style.background = divTime > new Date().getTime() ? 'orange' : 'teal'
})
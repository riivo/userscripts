// ==UserScript==
// @name         Retarus CSV
// @namespace    riivo
// @include      https://eas-email.retarus.com/*/security/live-search/*
// @require      https://code.jquery.com/jquery-3.5.1.min.js
// @version      6
// ==/UserScript==

const addButtons = async () => {
  while (!$('.event').length) {
    await new Promise(r => setTimeout(r, 500))
  }
  $('<button />', { css: { margin: '0 0 1em 1em' } })
    .addClass('btn btn-primary')
    .append('Simple CSV')
    .append($('<i class="fa fa-download ml-2" />'))
    .appendTo($('.search-tools'))
    .click(() => {
      event.preventDefault()
      let csv = 'timestamp;sender;recipient\n'
      $('.event-body').each((_i, event) => {
        csv += $('.event-date', event).text() + ';' +
          $('.eb-data', event).eq(0).text() + ';' +
          $('.eb-data', event).eq(1).text() + '\n'
      })
      $('<a />')
        .attr('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv))
        .attr('download', 'output.csv')
        .get(0).click()
    })
  $('<button />', { css: { margin: '0 0 1em 1em' } })
    .addClass('btn btn-primary')
    .append('Full TXT')
    .append($('<i class="fa fa-download ml-2" />'))
    .appendTo($('.search-tools'))
    .click(async () => {
      event.preventDefault()
      $('.fa-angle-down').trigger('click')
      await new Promise(r => setTimeout(r, 7000))
      let txt = ''
      $('.event').each((_i, event) => {
        $('li, .eb-data, *:not(:has(*), strong)', event).text((_i, t) => {
          txt += t && t.trim() + '\n'
        })
        txt += '\n'
      })
      $('<a />')
        .attr('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(txt))
        .attr('download', 'output.txt')
        .get(0).click()
    })
}

addButtons()
// ==UserScript==
// @name         Fitbit GPX
// @namespace    riivo
// @version      1
// @match        https://www.fitbit.com/activities/exercise/*
// @require      http://code.jquery.com/jquery-3.5.1.min.js
// @require      https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js
// @require      https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/vkbeautify/vkbeautify.0.99.00.beta.js
// @grant        GM_xmlhttpRequest
// @connect      web-api.fitbit.com
// ==/UserScript==

GM_xmlhttpRequest({
  method: 'GET',
  headers: {
    Authorization: 'Bearer ' + Cookies.get('oauth_access_token')
  },
  url: `https://web-api.fitbit.com/1.1/user/-/activities/${/\d+$/.exec(window.location.href)}.tcx`,
  onload: res => {
    const tcx = $.parseXML(res.responseText)
    const id = tcx.URL.match(/\d+$/)[0]
    const xml = document.implementation.createDocument('', '', null)
    const gpx = xml.createElement('gpx')
    gpx.setAttribute('version', '1.1')
    gpx.setAttribute('creator', 'https://gitlab.com/riivo')
    gpx.setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
    gpx.setAttribute('xmlns', 'http://www.topografix.com/GPX/1/1')
    gpx.setAttribute('xsi:schemaLocation',
      'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd')

    let wpt = xml.createElement('wpt')
    wpt.setAttribute('lat', $('LatitudeDegrees:first', tcx)[0].innerHTML)
    wpt.setAttribute('lon', $('LongitudeDegrees:first', tcx)[0].innerHTML)

    let time = xml.createElement('time')
    time.innerHTML = $('Time', tcx)[0].innerHTML.substring(0, 19) + 'Z'
    wpt.appendChild(time)

    let name = xml.createElement('name')
    name.innerHTML = 'Start'
    wpt.appendChild(name)

    let desc = xml.createElement('desc')
    desc.innerHTML = $('Time', tcx)[0].innerHTML.substring(0, 19).replace('T', ' ')
    wpt.appendChild(desc)

    gpx.appendChild(wpt)

    let lapCount = 1
    $('Lap', tcx).each(function () {
      wpt = xml.createElement('wpt')
      wpt.setAttribute('lat', $('LatitudeDegrees:last', this)[0].innerHTML)
      wpt.setAttribute('lon', $('LongitudeDegrees:last', this)[0].innerHTML)
      time = xml.createElement('time')
      time.innerHTML = $('Time:last', this)[0].innerHTML.substring(0, 19) + 'Z'
      wpt.appendChild(time)
      name = xml.createElement('name')
      name.innerHTML = 'Lap ' + lapCount++
      wpt.appendChild(name)
      desc = xml.createElement('desc')
      desc.innerHTML = Math.round(($('DistanceMeters', this)[0].innerHTML) / 10) / 100 + ' km'
      wpt.appendChild(desc)
      gpx.appendChild(wpt)
    })

    const trk = xml.createElement('trk')
    name = xml.createElement('name')
    name.innerHTML = 'From file ' + id + '.tcx'
    trk.appendChild(name)
    desc = xml.createElement('desc')
    desc.innerHTML = 'Created at'
    trk.appendChild(desc)
    const trkseg = xml.createElement('trkseg')
    $('Trackpoint', tcx).each(function () {
      const trkpt = xml.createElement('trkpt')
      trkpt.setAttribute('lat', $('LatitudeDegrees', this)[0].innerHTML)
      trkpt.setAttribute('lon', $('LongitudeDegrees', this)[0].innerHTML)
      const ele = xml.createElement('ele')
      ele.innerHTML = $('AltitudeMeters', this)[0].innerHTML
      trkpt.appendChild(ele)
      time = xml.createElement('time')
      time.innerHTML = $('Time', this)[0].innerHTML.substring(0, 19) + 'Z'
      trkpt.appendChild(time)
      trkseg.appendChild(trkpt)
    })

    trk.appendChild(trkseg)
    gpx.appendChild(trk)
    xml.appendChild(gpx)

    const xmlString = '<?xml version="1.0" encoding="utf-8"?>\n' +
      vkbeautify.xml((new XMLSerializer()).serializeToString(xml))
    $('<a />')
      .attr('href', 'data:application/xml;charset=utf-8,' + encodeURIComponent(xmlString))
      .attr('download', id + '.gpx')
      .appendTo('body')
      .get(0)
      .click()
  }
})
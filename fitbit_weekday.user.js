// ==UserScript==
// @name         Fitbit Weekday
// @namespace    riivo
// @match        https://www.fitbit.com/activities
// @grant        none
// ==/UserScript==

(async function () {
  while (!$('.logs time').length) {
    await new Promise(r => setTimeout(r, 500))
  }
  $('.logs time').text(function (_i, t) {
    return new Date($(this).attr('datetime')).toString().slice(0, 3) + ', ' + t
  })
})()
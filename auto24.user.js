// ==UserScript==
// @name        auto24
// @namespace   riivo
// @include     /(http|https):\/\/www.auto24.ee\/(used|kasutatud)\/(\d|auto).*/
// @include     /(http|https):\/\/www.mototehnika.ee\/(used|kasutatud)\/(\d|auto).*/
// @exclude     *auto_pildid.php*
// @require     http://code.jquery.com/jquery-3.5.1.min.js
// @version     4
// ==/UserScript==

const buttons = [
  {
    label: 'Maanteeamet',
    url: '\'https://eteenindus.mnt.ee/public/soidukTaustakontroll.jsf?regMark=\' + reg + \'&vin=\' + vin'
  }, {
    label: 'Google reg. number',
    url: '\'https://google.com/search?q=site:ee \' + reg'
  }, {
    label: 'Google VIN-kood',
    url: '\'https://google.com/search?q=\' + vin'
  }, {
    label: 'Liikluskahjud',
    url: '\'https://lkf.ee/kahjukontroll?reg=\' + reg + \'&vin=\' + vin'
  }
]

$('<div />', {
  id: 'buttonsDiv',
  css: { position: 'fixed', left: 0, top: '50%', width: '150px' }
}).appendTo('body')

$.each(buttons, (_, { label, url }) => {
  $('<button />', {
    html: label,
    click: () => {
      const reg = $('.field-reg_nr > .field').text()
      const vin = $('.field-tehasetahis > .field').text().slice(0, 17)
      if (reg.match('\\.\\.\\.') || vin.match('\\.\\.\\.')) {
        alert("VIN või reg. kood avamata")
      }
      else {
        window.open(eval(url))
      }
    }
  }).appendTo('#buttonsDiv')
})
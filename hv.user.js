// ==UserScript==
// @name        hv
// @namespace   riivo
// @include     https://foorum.hinnavaatlus.ee/viewforum.php?*
// @version     3
// ==/UserScript==

document.querySelectorAll('[src="templates/HinnaVaatlus/imagesHV/icon_newest_topic.gif"]')
  .forEach(icon =>
    icon.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAALCAYAAACd1bY6AAAAc0lEQVQ" +
    "okZ3S0QnAIAwEUJGM5jgdpxM4hEN0Jmn7UU7SeEr0434UXo6QUO/6rESidMFf2MHOq7ZokGJ6GmvEMIkyxiwIxGJ" +
    "H+dIwtgcLMgyQGwNoMQ2lvNEMDTTUYd6daSzlf6YYewM2ivvOZo2Wj9az1xdSfkCofvLwQAAAAABJRU5ErkJggg=="
  )